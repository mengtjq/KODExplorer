<?php 
//用户名密码
$config['user_name']="demo";
$config['user_password']="fe01ce2a7fbac8fafaed7c982a04e229";

//列表显示方式&排序方式
$config['listall']="list/icon";
$config['list']="icon";				// list||icon
$config['list_sort_field']="name";	// name||size||ext||mtime
$config['list_sort_order']="up";	// up||down

//文件列表主题
$config['themeall']="default=淡蓝(默认)/simple=简洁白/metro=metro风格";
$config['theme']="metro";		// 文件列表主题(规则，前面为css目录，后面为显示描述)

$config['codethemeall']="default/eclipse/github/monokai/solarized/ambiance/cobalt/dark";
$config['codetheme']="eclipse";	//代码编辑器主题

$config['wallall']="1=宇宙/2=炫彩紫/3=简约绿/4=生如夏花/5=夕阳草地/6=如梦如幻/7=草原/8=紫光/9=科技时代/10=旧时光/11=夕阳/12=科技方块/13=眩光紫/14=苹果/15=青草绿/16=梦幻落叶";
$config['wall']="1";//音乐播放主题
$config['musicthemeall']="1ting/beveled/kuwo/manila/mp3player/qqmusic/somusic/xdj";
$config['musictheme']="kuwo";//音乐播放主题
$config['moviethemeall']="webplayer/qqplayer/vplayer/tvlive/youtube";
$config['movietheme']="webplayer";//视频播放主题
